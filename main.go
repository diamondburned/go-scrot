package main

import (
	"image"
	"log"
	"os"

	"github.com/urfave/cli"
)

var (
	rect *image.Rectangle
	args = Args{
		NoOpenGL:    false,
		Destination: "/tmp/",
		Threshold:   "1MB",
		Clipboard:   false,
	}
)

// Args contains all possible args
type Args struct {
	NoOpenGL    bool
	Destination string
	Threshold   string
	Clipboard   bool
}

func init() {
	log.SetOutput(os.Stderr)
}

func main() {
	log.SetFlags(log.Lshortfile)

	app := cli.NewApp()
	app.Name = "go-scrot"
	app.Usage = "screenshot + slop wrapper"
	app.HideVersion = true
	app.EnableBashCompletion = true

	app.Flags = []cli.Flag{
		cli.BoolFlag{
			Name:        "noopengl, o",
			Usage:       "Tell slop not to use OpenGL",
			Destination: &args.NoOpenGL,
		},
		cli.BoolFlag{
			Name:        "clipboard, c",
			Usage:       "Put whatever is important in the clipboard",
			Destination: &args.Clipboard,
		},
		cli.StringFlag{
			Name:        "dest, d",
			Value:       "/tmp/",
			Usage:       "Set to write to `FILE` (if `ixio`, upload instead)",
			Destination: &args.Destination,
		},
		cli.StringFlag{
			Name:        "threshold, t",
			Value:       "1MB",
			Usage:       "`THRESHOLD` to store image into clipboard before storing it",
			Destination: &args.Threshold,
		},
	}

	app.Commands = []cli.Command{
		{
			Name:    "region",
			Aliases: []string{"r"},
			Usage:   "Take a regional screenshot",
			Action: func(c *cli.Context) error {
				rect, err := Select()
				if err != nil {
					return err
				}

				if err := Screenshot(rect); err != nil {
					return err
				}

				return nil
			},
		},
		{
			Name:    "screen",
			Aliases: []string{"f"},
			Usage:   "Take a full screenshot",
			Action: func(c *cli.Context) error {
				rect, err := Fullscreen()
				if err != nil {
					return err
				}

				if err := Screenshot(rect); err != nil {
					return err
				}

				return nil
			},
		},
	}

	app.Action = func(c *cli.Context) error {
		println("Unknown argument. Refer to ./go-scrot help.")
		os.Exit(1)

		return nil
	}

	err := app.Run(os.Args)
	if err != nil {
		panic(err)
	}
}
