# go-scrot

Wrapper around `slop` and `xclip`

## Dependencies

`slop` and `xclip`

## Why?

Because Bash is fucking boring. 

Also it's meant to replace `maim`, so go figure

## How to?

```bash
go get -u gitlab.com/diamondburned/go-scrot
PATH="$GOPATH/bin:$PATH"
./go-scrot -h
```

or get the binary from the CI (binary is statically compiled with musl and isn't stripped, so expect large size)

## Some things to note

- To upload to ixio, use `-d ixio`
- To output to stdout, use `-d -`
- To always use `-d`, use `-t 0`
