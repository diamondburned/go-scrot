package main

import (
	"bytes"
	"errors"
	"image"
	"image/png"
	"io/ioutil"
	"log"
	"mime/multipart"
	"net/http"
	"os"
	"os/exec"
	"strconv"
	"strings"

	"github.com/c2h5oh/datasize"
	"github.com/kbinani/screenshot"
)

// MIME type for clipboard
type MIME string

const (
	// PNG MIME type for clipboard
	PNG MIME = "image/png"

	// Text MIME type for clipboard
	Text MIME = "text/plain"
)

// Select a region using slop
func Select() (*image.Rectangle, error) {
	cmdArgs := []string{
		"slop", "-f", "%x\n%y\n%w\n%h",
	}

	if args.NoOpenGL {
		cmdArgs = append(cmdArgs, "-o")
	}

	cmd := exec.Command(cmdArgs[0], cmdArgs[1:]...)
	stdout, err := cmd.Output()
	if err != nil {
		return nil, err
	}

	dimensionArray := strings.Split(string(stdout), "\n")

	if len(dimensionArray) < 4 {
		println(string(stdout))
		return nil, errors.New("Invalid output")
	}

	x1 := procCoord(dimensionArray[0])
	y1 := procCoord(dimensionArray[1])
	x2 := procCoord(dimensionArray[2]) + x1
	y2 := procCoord(dimensionArray[3]) + y1

	rect := image.Rect(x1, y1, x2, y2)
	return &rect, nil
}

// Fullscreen or get the region for all screens
func Fullscreen() (*image.Rectangle, error) {
	disps := screenshot.NumActiveDisplays()
	if disps == 0 {
		return nil, errors.New("No displays found")
	}

	allbound := screenshot.GetDisplayBounds(0)

	for d := 1; d < disps; d++ {
		allbound = allbound.Intersect(
			screenshot.GetDisplayBounds(d),
		)
	}

	return &allbound, nil
}

// Screenshot or take a screenshot of the region
func Screenshot(region *image.Rectangle) error {
	img, err := screenshot.CaptureRect(*region)
	if err != nil {
		return err
	}

	image := new(bytes.Buffer)

	enc := &png.Encoder{
		CompressionLevel: png.BestCompression,
	}

	if err := enc.Encode(image, img); err != nil {
		log.Panic(err)
	}

	var threshold datasize.ByteSize
	if err := threshold.UnmarshalText([]byte(args.Threshold)); err != nil {
		return err
	}

	log.Println("Got", threshold.Bytes(), "from", args.Threshold, "for threshold")

	if uint64(image.Len()) > threshold.Bytes() {
		switch args.Destination {
		case "ixio", "0x0", "0x0.st":
			log.Println("Uploading to 0x0.st")

			body, err := to0x0(image)
			if err != nil {
				return err
			}

			log.Println(body)

			if args.Clipboard {
				buf := bytes.NewBufferString(body)
				toClipboard(*buf, Text)
			} else {
				println(body)
			}
		case "-":
			log.Println("Printing to stdout")
			image.WriteTo(os.Stdout)
		default:
			log.Println("Writing to", args.Destination)

			file, err := os.Create(args.Destination)
			if err != nil {
				return err
			}

			defer file.Close()

			image.WriteTo(file)

			if args.Clipboard {
				buf := bytes.NewBufferString(args.Destination)
				toClipboard(*buf, Text)
			} else {
				println(args.Destination)
			}
		}
	} else {
		log.Println("Writing to clipboard")

		if err := toClipboard(*image, PNG); err != nil {
			return err
		}
	}

	return nil
}

func procCoord(c string) int {
	i, err := strconv.Atoi(c)
	if err != nil {
		panic(err)
	}

	return i
}

func to0x0(w *bytes.Buffer) (string, error) {
	var formData = &bytes.Buffer{}
	form := multipart.NewWriter(formData)

	writer, err := form.CreateFormField("file")
	if err != nil {
		return "", err
	}

	if _, err = writer.Write(w.Bytes()); err != nil {
		return "", err
	}

	if err := form.Close(); err != nil {
		return "", err
	}

	request, err := http.NewRequest(
		"POST",
		"https://0x0.st",
		formData,
	)

	if err != nil {
		return "", err
	}

	request.Header.Set("Content-Type", "multipart/form-data")

	client := http.Client{}

	response, err := client.Do(request)
	if err != nil {
		return "", err
	}

	defer response.Body.Close()

	body, err := ioutil.ReadAll(response.Body)
	if err != nil {
		return "", err
	}

	return strings.TrimSpace(string(body)) + ".png", nil
}

func toClipboard(w bytes.Buffer, mime MIME) error {
	cmd := exec.Command(
		"xclip",
		"-selection", "clipboard",
		"-t", string(mime),
		"-i",
	)

	stdin, err := cmd.StdinPipe()
	if err != nil {
		panic(err)
	}

	defer stdin.Close()

	if err := cmd.Start(); err != nil {
		panic(err)
	}

	if _, err = w.WriteTo(stdin); err != nil {
		return err
	}

	return nil
}
